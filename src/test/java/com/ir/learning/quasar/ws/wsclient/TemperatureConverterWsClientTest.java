package com.ir.learning.quasar.ws.wsclient;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.ir.learning.quasar.model.ApiResponse;
import com.ir.learning.quasar.ws.config.WsClientConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WsClientConfig.class, TemperatureConverterWsClient.class})
public class TemperatureConverterWsClientTest {
	
	@Autowired
	private TemperatureConverterWsClient temperatureConverterWsClient;
	
	
	@Test
	public void convertTemperature_test() {
		double temp = -40;
		String unit = "C";
		ApiResponse response = temperatureConverterWsClient.convertTemperature(temp, unit);
		assertTrue(response.getMessage().equals("-40.0F"));
	}

}
