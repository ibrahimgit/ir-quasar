create sequence seq_user start with 1 increment by 1;
create sequence seq_role start with 1 increment by 1;
--password is password
--INSERT INTO T_USER(id, user_name, password, acc_expired, acc_locked, cred_expired, active) VALUES (1, 'ibrahim', '$2a$08$tcpPE6euA3aSR.FI.32pk.54lvKNmjapMLk1SjN6Ia4euzegxqpEm', 0, 0, 0, 1);
--INSERT INTO T_USER(id, user_name, password, acc_expired, acc_locked, cred_expired, active) VALUES (2, 'rashid', '$2a$08$tcpPE6euA3aSR.FI.32pk.54lvKNmjapMLk1SjN6Ia4euzegxqpEm', 0, 0, 0, 1);

--INSERT INTO T_ROLE(id, role_code, role_name) VALUES (1, 'ADMIN', 'Administration Role');
--INSERT INTO T_ROLE(id, role_code, role_name) VALUES (2, 'USER', 'Common User');

--INSERT INTO T_USER_ROLE(user_id, role_id) VALUES (1, 1);
--INSERT INTO T_USER_ROLE(user_id, role_id) VALUES (2, 2);