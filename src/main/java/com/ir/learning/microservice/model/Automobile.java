package com.ir.learning.microservice.model;

import java.time.LocalDate;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "automobile")
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_EMPTY)
public class Automobile {
	
	@XmlAttribute(name = "make")
	private String make;
	@XmlAttribute(name = "model")
	private String model;
	@XmlAttribute(name = "vehicleId")
	private Long vehicleId;
	//@XmlAttribute(name = "purchaseDate", required = false)
	@XmlTransient
	private LocalDate purchaseDate;
	@XmlAttribute(name = "engineNumber")
	private String engineNumber;
	@XmlElement(name = "options")
	private List<String> options;

}
