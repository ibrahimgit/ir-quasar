package com.ir.learning.microservice.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
@XmlRootElement(name = "applicant")
@XmlAccessorType(XmlAccessType.FIELD)
public class Applicant {
	
	@XmlElement(name = "fullName")
	private String fullName;
	@XmlElement(name = "sex")
	private String sex;
	@XmlElement(name = "city")
	private String city;
	@XmlElement(name = "martialStatus")
	private String martialStatus;
	
	

}
