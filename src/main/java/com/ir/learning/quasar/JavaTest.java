package com.ir.learning.quasar;

import java.util.List;
import java.util.Optional;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JavaTest {
	
	public static void main(String[] args) {
		String password = "password";
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder(8);
		String hashedPassword = bcrypt.encode(password);
		log.info("Hashed password: " + hashedPassword);
		log.info("matched: " + bcrypt.matches(password, hashedPassword));
		
		String veryOldHashedPassword = "$2a$08$.MjIZ7j2RvA.bhs0pnqMPeZ1BorDacZSViQrlXldox5Wy.xdVd5LG";
		log.info("very old matched: " + bcrypt.matches(password, veryOldHashedPassword));
		
		String oldhashedPassword = "$2a$08$fgp2mVDhSuBNNNbhzHEu8ecBo0nYiJCgAbin8dmnXXhqMEofA2Pce";
		log.info("old matched: " + bcrypt.matches(password, oldhashedPassword));
		
		String veryveryoldhashedPassword = "$2a$08$tcpPE6euA3aSR.FI.32pk.54lvKNmjapMLk1SjN6Ia4euzegxqpEm";
		log.info("old matched: " + bcrypt.matches(password, veryveryoldhashedPassword));
		
		String clientSecret = "secret";
		bcrypt = new BCryptPasswordEncoder(8);
		String hashedSecret = bcrypt.encode(clientSecret);
		log.info("Hashed clientSecret: " + hashedSecret);
		
		String userpassword = "password";
		bcrypt = new BCryptPasswordEncoder(6);
		String hashedUserPassword = bcrypt.encode(userpassword);
		log.info("Hashed hashedUserPassword: " + hashedUserPassword);
		
		log.info("bycrypt6 userpassword matched: " + bcrypt.matches(userpassword, hashedUserPassword));
		log.info("bycrypt6 clientSecret matched:  " + bcrypt.matches(clientSecret, hashedSecret));
		
		Optional<String> test = Optional.of("");
		test.ifPresent(a -> {});
		test.ifPresentOrElse(a -> {}, () -> {});
		
		List.of("");
	}

}
