package com.ir.learning.quasar.ws.wsclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ir.learning.quasar.model.ApiResponse;
import com.ir.learning.quasar.ws.config.SimpleWebServiceGateway;
import com.ir.learning.quasar.ws.generated.model.TempConversionRequest;
import com.ir.learning.quasar.ws.generated.model.TempConversionResponse;

@Component
public class TemperatureConverterWsClient {
	
	@Autowired
	private SimpleWebServiceGateway webServiceGateway;

	public ApiResponse convertTemperature(Double temp, String unit) {
		TempConversionRequest requestPayload = new TempConversionRequest();
		requestPayload.setTemp(temp);
		requestPayload.setDegree(unit);
		String uri = "http://localhost:8080/ir/microservice/ws/tempcoverter";
		TempConversionResponse response = webServiceGateway.invokeWs(uri, requestPayload);
		return ApiResponse.builder()
					.status(true)
					.message(response.getTemp()+response.getDegree())
					.build();
	}

}
