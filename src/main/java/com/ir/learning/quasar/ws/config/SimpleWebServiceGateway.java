package com.ir.learning.quasar.ws.config;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

public class SimpleWebServiceGateway extends WebServiceGatewaySupport {
	
	@SuppressWarnings("unchecked")
	public <T> T invokeWs(String uri, Object requestPayload) {
		return (T) getWebServiceTemplate().marshalSendAndReceive(uri, requestPayload);
	}
	
}
