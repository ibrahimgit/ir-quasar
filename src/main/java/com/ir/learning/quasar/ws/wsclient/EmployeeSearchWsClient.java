package com.ir.learning.quasar.ws.wsclient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ir.learning.quasar.ws.config.SimpleWebServiceGateway;
import com.ir.learning.quasar.ws.generated.model.Employee;
import com.ir.learning.quasar.ws.generated.model.EmployeeSearchRequest;
import com.ir.learning.quasar.ws.generated.model.EmployeeSearchResponse;

@Component
public class EmployeeSearchWsClient {
	
	@Autowired
	private SimpleWebServiceGateway webServiceGateway;

	
	public List<Employee> searchEmployees(Long empId) {
		String uri = "http://localhost:8080/ir/microservice/ws/employees/search";
		EmployeeSearchRequest requestPayload = new EmployeeSearchRequest();
		requestPayload.setEmpId(empId);
		EmployeeSearchResponse response = webServiceGateway.invokeWs(uri, requestPayload);
		return response.getEmployees();
	}
}
