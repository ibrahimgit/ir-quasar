package com.ir.learning.quasar.ws.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class WsClientConfig {
	
	/*
	 * Starting in Java 9, several key Java EE packages had their visibility
	 * reduced. These included core XML and SOAP-based packages. In Java 11, these
	 * packages have been removed altogether
	 * To use Spring Web Services on Java 9+, 
	 * you can no longer depend on the JDK providing key XML and SOAP-based libraries
	 */
	
	/*
	 * Steps to create web service client
	 * 1. Get a WSDL URL
	 * 2. Add Spring boot starter web service or spring-ws-core dependency in POM
	 * 3. Add maven-jaxb2-plugin plugin to generate java classes (request/response) from WSDL
	 * 		3a. Add JAXB and SOAP dependencies explicitly for JDK 11
	 * 4. Get a bean of Jaxb2Marshaller
	 * 5. Get bean of WebServiceGatewaySupport passing the marsheller ref bean
	 * 6. Autowire the WebServiceGatewaySupport bean and invoke web service passing URI and request body
	 */
	
	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.ir.learning.quasar.ws.generated.model");
		return marshaller;
	}
	
	@Bean
	public SimpleWebServiceGateway webServiceGateway(Jaxb2Marshaller marshaller) {
		SimpleWebServiceGateway webServiceGateway = new SimpleWebServiceGateway();
		webServiceGateway.setMarshaller(marshaller);
		webServiceGateway.setUnmarshaller(marshaller);
		return webServiceGateway;
	}

}
