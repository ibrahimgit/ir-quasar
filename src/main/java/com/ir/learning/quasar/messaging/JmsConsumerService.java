package com.ir.learning.quasar.messaging;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import com.ir.learning.microservice.model.Applicant;
import com.ir.learning.microservice.model.Automobile;
import com.ir.learning.microservice.model.Customer;

import lombok.extern.slf4j.Slf4j;



@Component
@Slf4j
public class JmsConsumerService {
	
	@JmsListener(destination = "${learning.jms.applicant-topic}" , containerFactory = "topicFactory" )
	public void messageConsumer1(Applicant applicant) {
		log.debug("applicant message receieved at subscriber 1", applicant); 
	}
	
	@JmsListener(destination = "${learning.jms.applicant-topic}" , containerFactory = "topicFactory" )
	public void messageConsumer2(Applicant applicant) {
		log.debug("applicant message receieved at subscriber 2");
		
	}
	
	@JmsListener(destination = "${learning.jms.applicant-topic}" , containerFactory = "topicFactory" )
	public void messageConsumer3(@Payload Applicant applicant, @Headers MessageHeaders headers, Message message) {
		log.debug("applicant message receieved at subscriber 3");
		log.debug("Headers: {}", headers);
		log.debug("Message: {}", message);
		
	}
	
	@JmsListener(destination = "${learning.jms.automobile-topic}" , containerFactory = "topicFactory" )
	public void autombileMessageConsumer1(Automobile automobile) {
		log.debug("automobile subscriber 1");
		
	}
	
	@JmsListener(destination = "${learning.jms.automobile-topic}" , containerFactory = "topicFactory" )
	public void autombileMessageConsumer2(Automobile automobile) {
		log.debug("automobile subscriber 2");
		
	}
	
	// if this is a queue, then both jmsTemplate and JmsListnerContainerFactory must be peer to peer domain
	@JmsListener(destination = "${learning.jms.customer-queue}" , containerFactory = "queueFactory" )
	public void queueMessageConsumer1(Customer customer) {
		log.debug("Customer subscriber 1");
		
	}
	
	// if this is a queue, then both jmsTemplate and JmsListnerContainerFactory must be peer to peer domain
	@JmsListener(destination = "${learning.jms.customer-queue}" , containerFactory = "queueFactory" )
	public void queueMessageConsumer2(Customer customer) {
		log.debug("Customer subscriber 2");
		
	}

}
