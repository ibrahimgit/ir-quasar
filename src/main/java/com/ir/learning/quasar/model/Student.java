package com.ir.learning.quasar.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Student {
	
	private String name;
	private String city;

}
