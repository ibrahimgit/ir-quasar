package com.ir.learning.quasar.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiResponse {
	
		private boolean status;
		private String message;
}
