package com.ir.learning.quasar.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ir.learning.quasar.model.ApiResponse;
import com.ir.learning.quasar.model.Faculty;
import com.ir.learning.quasar.ws.generated.model.Employee;
import com.ir.learning.quasar.ws.wsclient.EmployeeSearchWsClient;
import com.ir.learning.quasar.ws.wsclient.TemperatureConverterWsClient;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class InterserviceController {
	
	@Autowired
	private OAuth2RestTemplate oauth2RestTemplate;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private TemperatureConverterWsClient temperatureConverterWsClient;
	
	@Autowired
	private EmployeeSearchWsClient employeeSearchWsClient;
	
	@GetMapping("/publicapi/interservice/aouth2restcall")
	public ResponseEntity<List<Faculty>> getTeachers() {
		String url = "http://localhost:8070/ir/quasar/secureapi/teachers";
		log.info("Access token: {}", oauth2RestTemplate.getAccessToken()); // oauth2RestTemplate gets the token before hand, not sure how
		ResponseEntity<Faculty[]> response = oauth2RestTemplate.exchange(url, HttpMethod.GET, null, Faculty[].class);
		List<Faculty> responseBody = Arrays.asList(response.getBody());
		return ResponseEntity.ok(responseBody); 
	}
	
	
	@GetMapping("/publicapi/interservice/employees")
	public ResponseEntity<List<Employee>> getEmployees() {
		String url = "http://localhost:8080/ir/microservice/employees";
		ResponseEntity<Employee[]> response = restTemplate.exchange(url, HttpMethod.GET, null, Employee[].class);
		List<Employee> responseBody = Arrays.asList(response.getBody());
		return ResponseEntity.ok(responseBody); 
	}
	
	@GetMapping("/publicapi/interservice/tempConverter")
	public ApiResponse convertTemperature(@RequestParam("temp") Double temp, @RequestParam("unit") String unit) {
		return temperatureConverterWsClient.convertTemperature(temp, unit);
	}
	
	@GetMapping("/publicapi/interservice/employeesSearch")
	public List<Employee> searchEmployees(@RequestParam("empId") Long empId) {
		return employeeSearchWsClient.searchEmployees(empId);
	}

}
