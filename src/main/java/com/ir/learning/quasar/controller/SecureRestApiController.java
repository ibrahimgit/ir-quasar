package com.ir.learning.quasar.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ir.learning.quasar.model.ApiResponse;
import com.ir.learning.quasar.model.Faculty;
import com.ir.learning.quasar.model.Student;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class SecureRestApiController {
	
	
	@GetMapping("/secureapi/user") 
	public Principal secureApiUser(@AuthenticationPrincipal Principal principal, @AuthenticationPrincipal Authentication authentication) {
		log.info("User name from authentication: {}", authentication.getName());
		log.info("User name from Principal: {}", principal.getName());
		return principal; 
	}
	
	@GetMapping("/secureapi/teachers")
	@PreAuthorize("hasAuthority('ADMIN')") //Use hasAuthority, no Role_ prefix required
	public ResponseEntity<List<Faculty>> getTeachers(@AuthenticationPrincipal Principal user) {
		//log.info("User {} with role {} fetching teachers details",
		//		user.getUsername(), user.getAuthorities().iterator().next().getAuthority());
		Faculty faculty1 = Faculty.builder().name("ABC").subject("Physics").build();
		Faculty faculty2 = Faculty.builder().name("XYZ").subject("Math").build();
		
		return ResponseEntity.ok(Arrays.asList(faculty1, faculty2)); 
	}
	
	@GetMapping("/secureapi/students")
	@PreAuthorize("hasAuthority('USER')") //Use hasAuthority, no Role_ prefix required
	public ResponseEntity<List<Student>> getStudents(@AuthenticationPrincipal Principal user) {
		//log.info("User {} with role {} fetching students details",
		//		user.getUsername(), user.getAuthorities().iterator().next().getAuthority());
		Student student1 = Student.builder().name("AAA").city("Kolkata").build();
		Student student2 = Student.builder().name("YYY").city("Mumbai").build();
		
		return ResponseEntity.ok(Arrays.asList(student1, student2)); 
	}
	 
	@GetMapping("/secureapi/error") 
	public ApiResponse secureApiError(@RequestParam("divisor") Integer divisor) {
		int remainder = 10/divisor;
		return ApiResponse.builder().status(true).message("SUCCESS: " + remainder).build();
	}
}
