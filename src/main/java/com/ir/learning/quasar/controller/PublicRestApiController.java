package com.ir.learning.quasar.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ir.learning.quasar.model.ApiResponse;

@RestController
public class PublicRestApiController {
	
	@GetMapping("/publicapi")
	@PreAuthorize("isAnonymous() == true") //this means only anonymous user can access this method
	//any non secure (permitall) urls under spring security realm also need to get an authentication i.e AnonymousAuthenticationToken using AnonymousAuthenticationFilter
	public ApiResponse publicApi() {
		return ApiResponse.builder().status(true).message("SUCCESS").build();
	}
	
	@GetMapping("/publicapi/test/error") 
	public ApiResponse publicApiError(@RequestParam("divisor") Integer divisor) {
		int remainder = 10/divisor;
		return ApiResponse.builder().status(true).message("SUCCESS: " + remainder).build();
	}

}
