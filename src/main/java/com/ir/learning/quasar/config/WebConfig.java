package com.ir.learning.quasar.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
	private final Integer READ_TIMEOUT = 5000;
	private final Integer CONNECTION_TIMEOUT = 1000;
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
			.allowedOrigins("*")
			.allowedMethods("*")
			.allowedHeaders("*");
	}
	
	@Bean
	public OAuth2RestTemplate oauth2restTemplate() {
		OAuth2ProtectedResourceDetails resource = oAuth2ProtectedResourceDetails();
		OAuth2RestTemplate oAuth2RestTemplate = new OAuth2RestTemplate(resource);
		oAuth2RestTemplate.setRequestFactory(getClientHttpRequestFactory());
		return oAuth2RestTemplate;
	}

	@Bean
	@ConfigurationProperties(prefix = "security.oauth2.client")
	public ClientCredentialsResourceDetails oAuth2ProtectedResourceDetails() {
		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
		return resource;
	}
	
	@Bean
	public RestTemplate restTemplate() {
		// RestTemplate with no arg constructor uses SimpleClientHttpRequestFactory which uses standard JDK client
		// i.e HttpURLConnection
		RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
		//restTemplate.setInterceptors(Arrays.asList(a));
		return restTemplate;
	}

	private ClientHttpRequestFactory getClientHttpRequestFactory() {
		// The below factory uses Apache HttpComponentsHttpClient to create requests.  
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setReadTimeout(READ_TIMEOUT);
		requestFactory.setConnectTimeout(CONNECTION_TIMEOUT);
		// Connection timeout: max time allowed to establish a TCP connection between source and target
		// Read timeout / Socket timeout: max time allowed to read next data byte from a host after establishing the connection 
		return requestFactory;
	}
	
	
}
