package com.ir.learning.quasar.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.ir.learning.quasar.exception.model.ErrorResponse;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
	
	@ExceptionHandler(AuthenticationException.class)
	public ResponseEntity<ErrorResponse> authentication(AuthenticationException e) {
		log.error("{}", e);
		HttpStatus httpsStatus = HttpStatus.UNAUTHORIZED;
		return getErrorResponse(e.getMessage(), httpsStatus, httpsStatus.value());
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> genericException(Exception e) {
		log.error(e.getMessage(), e);
		HttpStatus httpsStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		//always return response entity from controller advice class
		return getErrorResponse(e.getMessage(), httpsStatus, httpsStatus.value());
	}

	private ResponseEntity<ErrorResponse> getErrorResponse(String message, HttpStatus httpsStatus, int errorCode) {
		return ResponseEntity.status(httpsStatus).body(new ErrorResponse(message, httpsStatus, errorCode));
	}

}
