pipeline {
	//https://jenkins.io/doc/book/pipeline/
	//https://www.edureka.co/blog/jenkins-pipeline-tutorial-continuous-delivery
	//https://gist.github.com/merikan/228cdb1893fca91f0663bab7b095757c [example]
    agent any
    
    tools {
    	maven 'Maven'
    //  jdk 'Java8'
    	jdk 'Jdk11'
    }
    
    environment {
    	GITLAB_CREDENIALS_ID = 'gitlab_credentials'
    }
    
    stages {
        stage('Compiling & packaging') {
            steps {
            	echo 'compiling, testing & packaging the project'
                script {
                    if(isUnix()) {
                        sh 'mvn clean install -Dmaven.test.failure.ignore=true'
                    } else {
                        bat 'mvn clean install -Dmaven.test.failure.ignore=true'
                    }
                }
            }
            post {
        		always {
        			echo "collecting Junit report"
                                script {
                                    if(fileExists("target/surefire-reports/")) {
                                        junit 'target/surefire-reports/*.xml'
                                    }
                                }
        		}
        	}
        }
        
        stage ('Quality gate check') {
            steps {
        	echo 'running sonar'
                script {
                    if(isUnix()) {
                        sh 'mvn sonar:sonar'
                    } else {
                        bat 'mvn sonar:sonar'
                    }

        	}
	    }
        }
        stage ('Deployment') {
        	steps {
        		script {
                                if(isUnix()) {

                                        echo 'deploying the project on Unix based platform'
        				sh '''
						cloudDeeployment=true
						portNumber=8070
						    
        				   	if $cloudDeeployment; then
        				   	    	echo "transferring jar to AWS EC2"
        				   	    	scp -C -o StrictHostKeyChecking=no  -i /var/lib/jenkins/workspace/ir-key.pem target/*.jar ubuntu@ec2-3-222-3-156.compute-1.amazonaws.com:/home/ubuntu/deployment/
        				   	    echo "jar file transferred to AWS EC2"
        				   	    
        				   		ssh -o StrictHostKeyChecking=no  -i /var/lib/jenkins/workspace/ir-key.pem  ubuntu@ec2-3-222-3-156.compute-1.amazonaws.com /home/ubuntu/deployment/deployment_script.sh $portNumber ir-quasar
        				   	    
							echo "application started successfully"
							
						else
                                                	deploymentDir='/home/ibrahim/workspace/deployment/ir-quasar'
                                                	pid=$(lsof -t -i:$portNumber) || echo "no process found running on port number $portNumber"

                                                	if [ -n "$pid" ]; then
                                                        	echo "stopping existing running application on port number $portNumber with process id $pid"
                                                        	kill -9 "$pid" || echo "failed to stop application on port number $portNumber"
                                                	else
                                                        	echo "No applicaton running on port number $portNumber, skipping stop"
                                                	fi
                                                	echo 'removng old jar (if any)...'
                                                	rm -f $deploymentDir/*.jar
                                                	echo 'copy new jar file'
                                                	cp target/*.jar $deploymentDir/ir-quasar.jar
                                                	echo 'update execution permission'
                                                	chmod 777 $deploymentDir/ir-quasar.jar
                                                	echo 'starting application'
                                                	cd $deploymentDir
                                                	JENKINS_NODE_COOKIE=dontKillMe nohup nice java -jar ir-quasar.jar &
                                                	echo $! > $deploymentDir/pid.file
                                                	echo "Application started with process id: $(cat $deploymentDir/pid.file)"
						fi
                                        '''
                                } else {
                                        echo 'deploying the project on windos platform'
                
                       			bat '''
                                                set "workspace=%cd%"
                                                set "deploymentDir=D:\\Personal workspace\\deployment\\ir-quasar"
                                                "%deploymentDir%/winsw-deployment.exe" stop
                                                "%deploymentDir%/winsw-deployment.exe" uninstall
                                                echo f | xcopy /Y target\\*.jar "%deploymentDir%\\ir-quasar.jar"
                                                "%deploymentDir%/winsw-deployment.exe" install
                                                "%deploymentDir%/winsw-deployment.exe" start
                                        '''

                                }
                        }
        	}
        }
    }
    
    post {
        always {
            echo "Build result: ${currentBuild.currentResult}"
        }
        failure {
            echo "The  build number ${env.BUILD_NUMBER} of ${currentBuild.fullDisplayName} is failed"
        }
        success {
            echo "The  build number ${env.BUILD_NUMBER} of ${currentBuild.fullDisplayName} is successful"
        }
        unstable {
            echo "This will run only if the marked was marked as unstable"
        }
        changed {
            echo 'This will run only if the state of the pipeline has changed'
            echo 'For example, If the pipeline was previously failing but is now sucessful'
        }
    }
}
